extern crate newb_manifest;

#[macro_use]
extern crate clap;

use clap::{App, AppSettings, ArgMatches};
use newb_manifest::Manifest;

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml)
        .setting(AppSettings::ArgRequiredElseHelp)
        .get_matches();

    match matches.subcommand_name() {
        Some("generate") => generate(matches),
        Some("compare") => compare(matches),
        _ => println!("Sub command is required"),
    }
}

fn generate(matches: ArgMatches) {
    let sub_matches = matches.subcommand_matches("generate").unwrap();
    let source = sub_matches.value_of("SOURCE").unwrap();
    let mut manifest = Manifest::new(String::from(source)).unwrap();
    manifest.generate_with_closure(|filename, hash| {
        println!("{}: {}", hash, filename);
    });
}

fn compare(matches: ArgMatches) {
    let sub_matches = matches.subcommand_matches("compare").unwrap();
    let source = sub_matches.value_of("SOURCE").unwrap();
    let target = sub_matches.value_of("TARGET").unwrap();

    let mut manifest_source = Manifest::read(String::from(source)).unwrap();
    let mut manifest_target = Manifest::read(String::from(target)).unwrap();
    manifest_source.compare_with_closure(&manifest_target, |filename, hash| {
        println!("{}: {}", filename, hash);
    });
}
