extern crate checksums;
extern crate fs_utils;
extern crate walkdir;

use std::collections::HashMap;
use std::fs::read_to_string;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::{Path, PathBuf};

use checksums::hash_file;
use checksums::Algorithm;
use walkdir::WalkDir;

#[derive(Debug)]
pub struct ManifestError {
    kind: String,
    message: String,
}

impl From<io::Error> for ManifestError {
    fn from(error: io::Error) -> Self {
        ManifestError {
            kind: String::from("io"),
            message: error.to_string(),
        }
    }
}

/// The manifest structure reflects a given directory's files with their individual hash.
/// Saving it will generate a manifest file at the root of the source directory with the details.
/// The default algorithm is MD5 but can be changed with `Manifest::new_with_algo()`.
///
/// # Example
/// ```
///   # extern crate test_dir;
///   extern crate newb_manifest;
///   use newb_manifest::Manifest;
///   # use test_dir::{TestDir, FileType, DirBuilder};
///
///   let mut source_dir = String::from("/source");
///   # let temp = TestDir::temp()
///   #     .create("source", FileType::Dir)
///   #     .create("source/directory", FileType::Dir)
///   #     .create("source/directory/empty_file", FileType::EmptyFile);
///   # source_dir = temp.root().display().to_string();
///   # source_dir.push_str("/source");
///   let mut manifest = Manifest::new(source_dir).unwrap();
///   manifest.generate();
///   manifest.save();
/// ```
///
pub struct Manifest {
    algo: Algorithm,
    source_dir: String,
    files: HashMap<String, String>,
}

impl Manifest {
    fn check_source(mut source_dir: String) -> Result<String, ManifestError> {
        if source_dir.is_empty() {
            return Err(ManifestError {
                kind: String::from("io"),
                message: format!("Source directory cannot be an empty string: {}", source_dir),
            });
        }

        let path = Path::new(&source_dir);
        if !path.exists() {
            return Err(ManifestError {
                kind: String::from("io"),
                message: format!("Source directory does not exists: {}", source_dir),
            });
        }

        if !path.is_dir() {
            return Err(ManifestError {
                kind: String::from("io"),
                message: format!("Source directory is not a directory: {}", source_dir),
            });
        }

        if fs_utils::check::is_folder_empty(path).unwrap() {
            return Err(ManifestError {
                kind: String::from("io"),
                message: format!("Source directory is empty: {}", source_dir),
            });
        }

        //      TODO: Does not work here?   if source_dir.ends_with('/') {
        if source_dir.chars().last().unwrap() != '/' {
            source_dir.push('/');
        }

        Ok(source_dir)
    }

    pub fn new(mut source_dir: String) -> Result<Manifest, ManifestError> {
        source_dir = Self::check_source(source_dir)?;

        Ok(Manifest {
            algo: Algorithm::MD5,
            source_dir,
            files: HashMap::new(),
        })
    }

    pub fn new_with_algo(
        mut source_dir: String,
        algo: Algorithm,
    ) -> Result<Manifest, ManifestError> {
        source_dir = Self::check_source(source_dir)?;

        Ok(Manifest {
            algo,
            source_dir,
            files: HashMap::new(),
        })
    }

    pub fn read(mut source_dir: String) -> Result<Manifest, ManifestError> {
        source_dir = Self::check_source(source_dir)?;
        let mut manifest_file = source_dir.clone();
        manifest_file.push_str("manifest");

        let path = Path::new(&manifest_file);
        if !path.exists() {
            return Err(ManifestError {
                kind: String::from("io"),
                message: format!(
                    "Source directory does not contain a manifest file: {}",
                    source_dir
                ),
            });
        }

        let content = read_to_string(path)?;
        let mut lines = content.split('\n');
        let algo = lines.next().unwrap().parse().unwrap();
        let mut files = HashMap::new();

        for line in lines {
            let parts = line.split(": ").collect::<Vec<_>>();
            if parts.len() < 2 {
                continue;
            }

            files.insert(String::from(parts[1]), String::from(parts[0]));
        }

        Ok(Manifest {
            algo,
            source_dir,
            files,
        })
    }

    /// Populates the internal HashMap `Manifest::files` with `<filename, hash>`.
    /// To access this internal property use `Manifest.get_files()`.
    pub fn generate(&mut self) {
        self.generate_with_closure(|filename, hash| {})
    }

    pub fn generate_with_closure<F>(&mut self, closure: F)
    where
        F: Fn(&String, &String),
    {
        let source_dir_len = self.source_dir.len();
        let walker = WalkDir::new(&self.source_dir).follow_links(false);
        for entry in walker.into_iter().filter_map(Result::ok) {
            let filename = entry.path().display().to_string().split_off(source_dir_len);
            if filename == "/manifest" || filename.is_empty() {
                continue;
            }

            let mut hash: String;
            if entry.file_type().is_symlink() {
                hash = String::from("SYMLINK");
            } else if entry.file_type().is_dir() {
                hash = String::from("DIRECTORY");
            } else {
                hash = hash_file(entry.path(), self.algo);
            }

            closure(&filename, &hash);

            self.files.insert(filename, hash);
        }
    }

    /// Returns the internal files that the function `Manifest.generate()`
    /// will compute. Thus it needs ot be called before `Manifest.get_files()` is called.
    /// This returns **a copy** of the internal list of files.
    ///
    /// # Example
    /// ```
    ///   # extern crate test_dir;
    ///   extern crate newb_manifest;
    ///   use newb_manifest::Manifest;
    ///   # use test_dir::{TestDir, FileType, DirBuilder};
    ///
    ///   let mut source_dir = String::from("/source");
    ///   # let temp = TestDir::temp()
    ///   # .create("source", FileType::Dir)
    ///   #     .create("source/directory1", FileType::Dir)
    ///   #     .create("source/directory1/file1", FileType::RandomFile(10))
    ///   #     .create("source/directory1/file2", FileType::RandomFile(20))
    ///   #     .create("source/directory1/file3", FileType::RandomFile(30))
    ///   #     .create("source/directory2", FileType::Dir)
    ///   #     .create("source/directory2/file1", FileType::RandomFile(10))
    ///   #     .create("source/directory3", FileType::Dir);
    ///   # source_dir = temp.root().display().to_string();
    ///   # source_dir.push_str("/source");
    ///   let mut manifest = Manifest::new(source_dir).unwrap();
    ///   manifest.generate();
    ///
    ///   println!("{:#?}", manifest.get_files());
    /// ```
    ///
    /// Which will output something along the lines of:
    /// ```text
    /// {
    ///     "directory1": "DIRECTORY",
    ///     "directory1/file1": "E57BC1D85B05EF4B1BC32BB46837179D",
    ///     "directory1/file2": "59081F052A207FD6BC7CAE865FDB9255",
    ///     "directory1/file3": "510E94D9993A474DD35DBE52C5B8F467",
    ///     "directory2": "DIRECTORY",
    ///     "directory2/file1": "E6F62ACFBF05B5A6F9A7E0E708E7B249",
    ///     "directory3": "DIRECTORY",
    /// }
    /// ```
    pub fn get_files(&self) -> HashMap<String, String> {
        self.files.clone()
    }

    /// Creates a `manifest` file at the root of the directory with the details of the internal
    /// property files. The function will Err out if the source directory was never scanned, thus
    /// having the internal property `files` being empty.
    ///
    /// Example of the file generated:
    /// ```text
    ///     MD5
    ///     "DIRECTORY": "directory1"
    ///     "E57BC1D85B05EF4B1BC32BB46837179D": "directory1/file1"
    ///     "59081F052A207FD6BC7CAE865FDB9255": "directory1/file2"
    ///     "510E94D9993A474DD35DBE52C5B8F467": "directory1/file3"
    ///     "DIRECTORY": "directory2"
    ///     "E6F62ACFBF05B5A6F9A7E0E708E7B249": "directory2/file1"
    ///     "DIRECTORY": "directory3"
    /// ```
    pub fn save(&self) -> Result<(), ManifestError> {
        if self.files.len() <= 0 {
            return Err(ManifestError {
                kind: String::from("internal"),
                message: String::from("The source directory have not yet been scanned, call Manifest::generate() before calling save"),
            });
        }

        let mut source_dir = self.source_dir.clone();
        source_dir.push_str("manifest");

        let mut manifest_file = File::create(source_dir)?;
        manifest_file.write_all(&format!("{:?}\n", &self.algo).into_bytes())?;
        for (filename, hash) in &self.files {
            manifest_file.write_all(&format!("{}: {}\n", hash, filename).into_bytes())?;
        }

        manifest_file.sync_all()?;
        Ok(())
    }

    pub fn compare(&self, manifest: &Manifest) -> Result<HashMap<String, String>, ManifestError> {
        self.compare_with_closure(&manifest, |filename, hash| {})
    }

    pub fn compare_with_closure<F>(
        &self,
        manifest: &Manifest,
        closure: F,
    ) -> Result<HashMap<String, String>, ManifestError>
    where
        F: Fn(&String, &String),
    {
        if self.algo != manifest.algo {
            return Err(ManifestError {
                kind: String::from("io"),
                message: format!(
                    "Algorithm does not match: {:?} != {:?}",
                    self.algo, manifest.algo
                ),
            });
        }

        let mut files_diff = HashMap::new();
        for (filename, hash) in &self.files {
            if filename.is_empty() {
                continue;
            }

            if !manifest.files.contains_key(filename.as_str()) {
                let action = String::from("DELETED");
                closure(&filename, &action);
                files_diff.insert(filename.clone(), action);
                continue;
            }

            if hash.as_str() != manifest.files[filename].as_str() {
                let action = String::from("MODIFIED");
                closure(&filename, &action);
                files_diff.insert(filename.clone(), action);
            }
        }

        for (filename, _) in &manifest.files {
            if filename.is_empty() {
                continue;
            }

            if !self.files.contains_key(filename.as_str()) {
                let action = String::from("ADDED");
                closure(&filename, &action);
                files_diff.insert(filename.clone(), action);
            }
        }

        Ok(files_diff)
    }
}

#[cfg(test)]
mod tests {
    extern crate test_dir;

    use super::*;
    use test_dir::{DirBuilder, FileType, TestDir};

    #[test]
    #[should_panic(expected = "Source directory cannot be an empty string: ")]
    fn source_dir_empty_string() {
        Manifest::new(String::from("")).unwrap();
    }

    #[test]
    #[should_panic(expected = "Source directory does not exists: /dir1/dir2/dir3")]
    fn source_dir_ends_dir_not_found() {
        Manifest::new(String::from("/dir1/dir2/dir3")).unwrap();
    }

    #[test]
    #[should_panic(
        expected = "The source directory have not yet been scanned, call Manifest::generate() before calling save"
    )]
    fn save_files_empty() {
        let temp = TestDir::temp()
            .create("test", FileType::Dir)
            .create("test/empty_file", FileType::EmptyFile);

        let mut source_dir = temp.root().display().to_string();
        source_dir.push_str("/test");

        let mfest = Manifest::new(source_dir.clone()).unwrap();
        mfest.save().unwrap();
    }

    #[test]
    fn source_dir_empty_dir() -> Result<(), String> {
        let temp = TestDir::temp().create("test", FileType::Dir);
        let mut source_dir = temp.root().display().to_string();
        source_dir.push_str("/test");

        if let Err(e) = Manifest::new(source_dir.clone()) {
            assert_eq!(
                e.message,
                format!("Source directory is empty: {}", source_dir)
            );
            return Ok(());
        }

        Err(String::from(
            "Manifest::new() should Err if source dir is empty",
        ))
    }

    #[test]
    fn source_dir_file_path() -> Result<(), String> {
        let temp = TestDir::temp()
            .create("test", FileType::Dir)
            .create("test/empty_file", FileType::EmptyFile);

        let mut source_dir = temp.root().display().to_string();
        source_dir.push_str("/test/empty_file");

        if let Err(e) = Manifest::new(source_dir.clone()) {
            assert_eq!(
                e.message,
                format!("Source directory is not a directory: {}", source_dir)
            );
            return Ok(());
        }

        Err(String::from(
            "Manifest::new() should Err if source dir should not point to a file",
        ))
    }

    #[test]
    fn source_dir_ends_with_one_slash() {
        let temp = TestDir::temp()
            .create("test", FileType::Dir)
            .create("test/empty_file", FileType::EmptyFile);

        // No slash
        let mut source_dir = String::from(temp.root().to_str().unwrap());
        source_dir.push_str("/test");

        let mfest = Manifest::new(source_dir.clone()).unwrap();

        source_dir.push('/');
        assert_eq!(source_dir, mfest.source_dir);

        // With slash
        let mut source_dir2 = String::from(temp.root().to_str().unwrap());
        source_dir2.push_str("/test/");

        let mfest2 = Manifest::new(source_dir2.clone()).unwrap();

        assert_eq!(source_dir2, mfest2.source_dir);
    }

    fn create_test_source_dir() -> TestDir {
        TestDir::temp()
            .create("test", FileType::Dir)
            .create("test/random_file", FileType::RandomFile(50))
            .create("test/dir1", FileType::Dir)
            .create("test/dir1/random_file1", FileType::RandomFile(10))
            .create("test/dir1/random_file2", FileType::RandomFile(20))
            .create("test/dir1/random_file3", FileType::RandomFile(30))
            .create("test/dir1/random_file4", FileType::RandomFile(40))
            .create("test/dir2", FileType::Dir)
            .create("test/dir1/dir3", FileType::Dir)
            .create("test/dir1/dir3/random_file1", FileType::RandomFile(10))
            .create("test/dir1/dir3/random_file2", FileType::RandomFile(20))
            .create("test/dir1/dir3/random_file3", FileType::RandomFile(30))
            .create("test/dir1/dir3/random_file4", FileType::RandomFile(40))
            .create("test/dir1/dir3/dir4", FileType::Dir)
    }

    #[test]
    fn generate_dirs_symlinks() {
        let test_dir = create_test_source_dir();
        let mut source_dir = String::from(test_dir.root().to_str().unwrap());
        source_dir.push_str("/test");

        let mut mfest = Manifest::new(source_dir.clone()).unwrap();
        mfest.generate();

        assert!(mfest.files.len() > 0);
        for (filename, hash) in mfest.files {
            let mut file = String::from("test/");
            file.push_str(filename.as_str());

            if (filename.is_empty()) {
                continue;
            }

            let path = test_dir.path(file.as_str());
            if path.symlink_metadata().unwrap().file_type().is_symlink() {
                assert_eq!(String::from("SYMLINK"), hash);
                continue;
            }

            if path.is_dir() {
                assert_eq!(String::from("DIRECTORY"), hash);
                continue;
            }

            assert!(path.exists());
            assert!(path.is_file());
        }
    }

    fn generate_manifest_file(test_dir: &TestDir) -> Result<Manifest, ManifestError> {
        let mut source_dir = String::from(test_dir.root().to_str().unwrap());
        source_dir.push_str("/test");

        let mut mfest = Manifest::new(source_dir.clone()).unwrap();
        mfest.generate();
        mfest.save();

        Ok(mfest)
    }

    #[test]
    fn save_manifest_file() -> Result<(), std::io::Error> {
        let test_dir = create_test_source_dir();
        let mut mfest = generate_manifest_file(&test_dir).unwrap();

        let mut mainfest_file = mfest.source_dir.clone();
        mainfest_file.push_str("manifest");
        let path = test_dir.path(mainfest_file.as_str());

        assert!(path.exists());
        assert!(path.is_file());

        let content = read_to_string(path)?;
        let mut lines = content.split('\n');

        let algo = lines.next().unwrap().parse().unwrap();
        assert_eq!(mfest.algo, algo);

        // We add one because of the algorithm first line
        assert_eq!(mfest.files.len() + 1, lines.count());

        Ok(())
    }

    #[test]
    fn read_manifest_file() -> Result<(), std::io::Error> {
        let test_dir = create_test_source_dir();
        let mut mfest = generate_manifest_file(&test_dir).unwrap();

        let rfest = Manifest::read(mfest.source_dir.clone()).unwrap();
        assert_eq!(mfest.algo, rfest.algo);
        assert_eq!(mfest.source_dir, rfest.source_dir);
        assert_eq!(mfest.files, rfest.files);

        Ok(())
    }

    #[test]
    fn compare_manifest() {
        let test_dir1 = create_test_source_dir();
        let mut mfest1 = generate_manifest_file(&test_dir1).unwrap();

        let mut test_dir2 = create_test_source_dir()
            .remove("test/random_file")
            .remove("test/dir1/dir3")
            .remove("test/dir1/random_file3")
            .create("test/dir1/random_file3", FileType::RandomFile(40))
            .create("test/dir1/added_random_file", FileType::RandomFile(40));

        let mut mfest2 = generate_manifest_file(&test_dir2).unwrap();
        let result = mfest1.compare(&mfest2).unwrap();

        assert_eq!(result["random_file"], String::from("DELETED"));
        assert_eq!(result["dir1/dir3"], String::from("DELETED"));
        assert_eq!(result["dir1/random_file3"], String::from("MODIFIED"));
        assert_eq!(result["dir1/added_random_file"], String::from("ADDED"));
    }
}
